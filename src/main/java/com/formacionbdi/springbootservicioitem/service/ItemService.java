package com.formacionbdi.springbootservicioitem.service;

import com.formacionbdi.springbootservicioitem.models.Item;

import java.util.List;

public interface ItemService {

    public List<Item> findAll();

    public Item findById(Long id, Integer cantidad);

}
